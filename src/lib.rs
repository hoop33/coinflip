use rand::{Rng, thread_rng};

#[derive(Debug)]
pub struct Coin {
    faces: String,
}

impl Coin {
    pub fn new(faces: String) -> Self {
        Self { faces }
    }

    pub fn flip(&self, count: u32) -> String {
        (0..count)
            .map(|_| {
                let face = thread_rng().gen_range(0..self.faces.len());
                self.faces.chars().nth(face).unwrap()
            })
            .collect()
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn flip_should_return_string_with_correct_length() {
        let coin = Coin::new("HT".to_string());
        let result = coin.flip(10);
        assert_eq!(result.len(), 10);
    }

    #[test]
    fn flip_should_return_string_with_correct_characters() {
        let coin = Coin::new("HT".to_string());
        let result = coin.flip(1000);
        // OK, we _could_ get 1000 Heads or 1000 Tails, but c'mon.
        assert!(result.contains("H") && result.contains("T"));
    }

    #[test]
    fn flip_should_return_string_with_at_least_one_heads() {
        let coin = Coin::new("HT".to_string());
        let result = coin.flip(1000);
        let count = result.matches("H").count();
        assert!(count > 0 && count < 1000);
    }

    #[test]

    fn flip_should_return_string_with_at_least_one_tails() {
        let coin = Coin::new("HT".to_string());
        let result = coin.flip(1000);
        let count = result.matches("T").count();
        assert!(count > 0 && count < 1000);
    }

    #[test]
    fn flip_should_return_string_with_correct_number_of_heads_and_tails() {
        let coin = Coin::new("HT".to_string());
        let result = coin.flip(1000);
        let count_heads = result.matches("H").count();
        let count_tails = result.matches("T").count();
        assert_eq!(1000, count_heads + count_tails);
    }

    #[test]
    fn flip_should_return_string_with_correct_number_of_results_when_faces_are_different() {
        let coin = Coin::new("abcdefghijklmnopqrstuvwxyz".to_string());
        let result = coin.flip(10000);
        assert_eq!(10000, result.len());
    }
}