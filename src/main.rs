use clap::Parser;
use coinflip::Coin;

/// Flip a coin, or any n-sided object
#[derive(Parser, Debug)]
#[command(version, about)]
struct Args {
    /// Number of times to flip the coin
    #[arg(short, long, default_value_t = 1)]
    count: u32,

    /// Faces on the coin
    #[arg(short, long, value_parser, default_value_t = String::from("HT"))]
    faces: String,
}

fn main() {
    let args = Args::parse();
    let coin = Coin::new(args.faces);
    print!("{}", coin.flip(args.count));
}
