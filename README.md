# coinflip

> Flips an n-sided coin any number of times

## Installation

- Install the Rust toolchain: <https://www.rust-lang.org/tools/install>
- Clone this repository: `git clone git@gitlab.com:hoop33/coinflip.git`
- Install: `cd coinflip && cargo install --path .`

## Usage

```console
$ coinflip --help
Flip a coin, or any n-sided object

Usage: coinflip [OPTIONS]

Options:
  -c, --count <COUNT>  Number of times to flip the coin [default: 1]
  -f, --faces <FACES>  Faces on the coin [default: HT]
  -h, --help           Print help
  -V, --version        Print version

$ coinflip
H

$ coinflip --count 100
TTTTHTHHTHHHHHHHHTHTTHTTHHTTHHTTHTTTHHHHHTHHHTTTHTTTTHTHTTHTTHHHTTTHTHTTHHHTHHTTTHTHHHHHHHTHTHTTTHHH

$ coinflip --faces 123456 --count 10
3155415232
```

Combine with standard shell commands to, say, flip a coin 100 times and get the counts of heads and tails:

```console
$ coinflip --count 100 | sed 's/./&\n/g' | sort | uniq -c
  50 H
  50 T
```

## License

Copyright &copy; 2024 Rob Warner

Licensed under the [MIT License](https://hoop33.mit-license.org/)
